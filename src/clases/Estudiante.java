package clases;

public class Estudiante {

    private String nombre;
    private double nota1;
    private double nota2;
    private double nota3;

    public Estudiante() {
        System.out.println("Alguien ha creado un objeto de tipo clase estudiante con el constructor 1.");
    }

    public Estudiante(String nombre, double nota1, double nota2, double nota3) {
        System.out.println("Alguien ha creado un objeto de tipo clase estudiante con el constructor 2.");
        this.nombre = nombre;
        this.nota1 = nota1;
        this.nota2 = nota2;
        this.nota3 = nota3;
    }

    public void mostrarResumen(String nombreEstudiante, double nota1Estudiante, double nota2Estudiante, double nota3Estudiante, double definitivaEstudiante) {
        String mensaje;
        mensaje = "**********************************************\n";
        mensaje += "RESUMEN DEL ESTUDIANTE \n";
        mensaje += "**********************************************\n";
        mensaje += "NOMBRE ESTUDIANTE: " + nombreEstudiante + "\n";
        mensaje += "NOTA 1: " + nota1Estudiante + "\n";
        mensaje += "NOTA 2: " + nota2Estudiante + "\n";
        mensaje += "NOTA 3: " + nota3Estudiante + "\n";
        mensaje += "NOTA DEFINITIVA: " + definitivaEstudiante + "\n";
        mensaje += "**********************************************\n";
        System.out.println(mensaje);
    }

    public double calcularDefinitiva() {
        double definitiva;
        definitiva = (nota1 + nota2 + nota3) / 3;
        return definitiva;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getNota1() {
        return nota1;
    }

    public void setNota1(double nota1) {
        this.nota1 = nota1;
    }

    public double getNota2() {
        return nota2;
    }

    public void setNota2(double nota2) {
        this.nota2 = nota2;
    }

    public double getNota3() {
        return nota3;
    }

    public void setNota3(double nota3) {
        this.nota3 = nota3;
    }

}
