package codigo;

import clases.Estudiante;
import java.util.Scanner;

public class Principal {

    public static void main(String[] args) {
        //******************************************
        //definicion de variables
        //******************************************
        int cantidadEstudiantes;
        String nombre;
        double nota1, nota2, nota3, definitiva;
        Scanner teclado;
        //******************************************
        //******************************************
        //creacion de objetos
        //******************************************
        teclado = new Scanner(System.in);
        //******************************************
        //******************************************
        //capturar datos por teclado
        //******************************************
        System.out.println("Cuantos estudiantes deseas calcular la nota: ");
        cantidadEstudiantes = teclado.nextInt();
        //******************************************
        //******************************************
        //logica de negocio
        //******************************************
        for (int i = 1; i <= cantidadEstudiantes; i++) {
           Estudiante objEstudiante;
           
            System.out.println("Ingrese el nombre del estudiante " + i + " : ");
            nombre = teclado.next();
            
            System.out.println("Ingrese la nota 1 del estudiante " + i + " : ");
            nota1 = teclado.nextDouble();
            
            System.out.println("Ingrese la nota 2 del estudiante " + i + " : ");
            nota2 = teclado.nextDouble();
            
            System.out.println("Ingrese la nota 3 del estudiante " + i + " : ");
            nota3 = teclado.nextDouble();
            
            objEstudiante = new Estudiante(nombre, nota1, nota2, nota3);
            
            objEstudiante.mostrarResumen(nombre, nota1, nota2, nota3, objEstudiante.calcularDefinitiva());
           
        }
        //********************************************

    }

}
